#include "mainwindow.h"
#include "ui_mainwindow.h"
void MainWindow::StartButtonClicked(){
  timer.start();
  currentFeed->start(QThread::HighestPriority); // This function sets the priority for a running thread. If the thread is not running, this function does nothing and returns immediately. Use start() to start a thread with a specific priority.
  currentFeed->running = true;

}

void MainWindow::onImageReceived(QImage img)
{
  ui->label1->setPixmap(QPixmap::fromImage(img)); // Qt provides four classes for handling image data: QImage, QPixmap, QBitmap and QPicture. QImage is designed and optimized for I/O, and for direct pixel access and manipulation, while QPixmap is designed and optimized for showing images on screen.
  std::cout << (1000 / (float)timer.elapsed()) << std::endl;
  timer.restart();
}

MainWindow::MainWindow(QWidget* parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow){
    ui->setupUi(this);

  currentFeed = new UVCThread(this);
  //Creates a connection of the given type from the signal in the sender object to the method in the receiver object. Returns a handle to the connection that can be used to disconnect it later.
  connect(
    currentFeed,
    SIGNAL(captureImage(QImage)),
    this,
    SLOT(onImageReceived(QImage)));

  connect(
    ui->MainWindow_Button_Start,
    &QPushButton::clicked,
    this,
    &MainWindow::StartButtonClicked);
}

MainWindow::~MainWindow() {
    delete ui;
}
